import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row>
			<Col>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>Learn from nyeerk</Card.Title>
						<Card.Text>
							Lorem Lorem Lorem 3x
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>Learn from nyeerk</Card.Title>
						<Card.Text>
							Lorem Lorem Lorem 3x
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			
			<Col>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>Learn from nyeerk</Card.Title>
						<Card.Text>
							Lorem Lorem Lorem 3x
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}