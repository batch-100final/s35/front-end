// base imports
import React, { useState, useEffect } from 'react';
import Router from 'next/router';

// bootstrap
import { Form, Button, Container } from 'react-bootstrap';

// export default allows the function to be used in other files
export default function Register() {
	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [isActive, setIsActive] = useState(true);


	function register(e) {
        // prevents page redirection
        e.preventDefault();
        fetch('http://localhost:4000/api/users/email-exists', {
            method: 'Post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            
            if(data === false){
                fetch('http://localhost:4000/api/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                    firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data) {
                    setFirstName('');
					setLastName('');
					setEmail('');
					setPassword1('');
                    setPassword2('');
					setMobileNo('');

                    alert('thank you for registering')

                    Router.push('/login');
                    }
                })
            }
        })
    }

    useEffect(() => {
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password1 !== '';
        let isPasswordConfirmNotEmpty = password2 !== '';
        let isPasswordMatched = password1 === password2;

        if (isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatched) {
            setIsActive(false);
        } else {
            setIsActive(true);
        }

    }, [email, password1, password2]);
    // useEffect(() => {
    // 	console.log(email);
    // }, [email]);

    // useEffect(() => {
    // 	console.log(password);
    // }, [password]);


	return (
		<Container>
			<h3>Register</h3>
			<Form onSubmit={(e) => register(e)}>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
					type="email" 
					placeholder="Enter email" 
					value={email} 
					onChange={(e) => setEmail(e.target.value)}
					required
				/> 
				</Form.Group> 

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter password" value={password1} onChange={(e) => setPassword1(e.target.value)} required/> 
				</Form.Group>

				<Form.Group>
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Confirm Passowrd" value={password2} onChange={(e) => setPassword2(e.target.value)} required/> 
				</Form.Group>

                <Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control 
					type="text" 
					placeholder="Enter First Name" 
					value={firstName} 
					onChange={(e) => setFirstName(e.target.value)}
					required
				/>
                </Form.Group>

                <Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control 
					type="text" 
					placeholder="Enter Last Name" 
					value={lastName} 
					onChange={(e) => setLastName(e.target.value)}
					required
				/>
                </Form.Group>

                <Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control 
					type="number" 
					placeholder="Enter contact number" 
					value={mobileNo} 
					onChange={(e) => setMobileNo(e.target.value)}
					required
				/>
                </Form.Group>

				<Button className="bg-primary" type="submit" disabled={isActive}>Submit</Button>

			</Form>
		</Container>
	)
}