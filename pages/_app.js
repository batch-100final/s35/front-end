import React, { useState, useEffect } from 'react';
import '../styles/globals.css'

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

import Navbar from '../components/Navbar';

// import Context provider
import { UserProvider } from '../UserContext';

export default function MyApp({ Component, pageProps }) {
	const[user, setUser] = useState({
		id: null,
		isAdmin: null
	})

	useEffect(() => {
		setUser({
			id: localStorage.getItem('id'),
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})
	}, [])


	// function for clearing local storage upon logout
	const unsetUser = () => {
		localStorage.clear();

		setUser({
			id: null,
			isAdmin: null
		})
	}

  return(
  	<React.Fragment>
  		<UserProvider value={{user, unsetUser, setUser}}>
  			<Navbar />
  				<Container>
  					<Component {...pageProps} />
  				</Container>
  		</UserProvider>
  	</React.Fragment>
  ) 
}


