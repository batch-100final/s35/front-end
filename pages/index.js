import React from 'react';
import Head from 'next/head';

import styles from '../styles/Home.module.css';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Navbar from '../components/Navbar';

export default function Home() {
  const data = {
    title: "Zuitt coding",
    content: "Content 1",
    destination: "/courses",
    label: "Enroll now!"
  }
  return (
    <React.Fragment>
       
      <Banner dataProp={data} />
      <Highlights />
    </React.Fragment>
  )
}
