// base imports
import { useContext, useState, useEffect } from 'react';
import Router from 'next/router';

import { Form, Button, Container } from 'react-bootstrap';

import UserContext from '../../UserContext';

import usersData from '../../data/userdata';

export default function Login() {
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isDisabled, setIsDisabled] = useState(true);
    
    function login(e) {
        e.preventDefault();
       
        if(email =='' || password == '' ){
            alert("Please input email/pass");
        } else {
            fetch('http://localhost:4000/api/users/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => {return res.json()})
            .then(data => {
                console.log(data); // token

                localStorage.setItem('token', data.accessToken);

                if(data !== null) {
                    fetch('http://localhost:4000/api/users/details', {
                        headers: {
                            Authorization: `Bearer ${data.accessToken}`
                        }
                    })
                    .then(res => res.json())
                    .then(data => {
                        console.log(data); // object details of user

                        localStorage.setItem('id', data._id);
                        localStorage.setItem('isAdmin', data.isAdmin);

                        // changes the value of the user state
                        setUser({
                            id: data._id,
                            isAdmin: data.isAdmin
                        })

                        alert('login successful');
                        Router.push('/courses');
                    })

                } else {
                    alert("Something went wrong");
                }
            })
        }
    }

	  useEffect(() => {
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';

        //Determine if all conditions have been met
        if (isEmailNotEmpty && isPasswordNotEmpty) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password]);

	  useEffect(() => {
        console.log(`User with an email: ${email} is an admin`)
    }, [user.isAdmin, email]) //user.email


	return (
		<Container fluid className="my-5">
                <h3>Login</h3>
                <Form onSubmit={login}>
                    <Form.Group>
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email} 
                            onChange={(e) => setEmail(e.target.value)} 
                            required
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            value={password} 
                            onChange={(e) => setPassword(e.target.value)} 
                            required
                        />
                    </Form.Group>
                    <Button variant="success" type="submit" disabled={isDisabled}>
                        Login
                    </Button>
                </Form>
            </Container>
	)
}
